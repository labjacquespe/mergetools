"""
Define commands that are built by the Job class from
the generator project, to create .sh script.

The path of executables that are called are also
defined here.
"""
import os.path

def script_dir():
    return os.path.dirname(__file__)

class bigWigMerge(object):
    """Generate several bigWigMerge commands.
    For each command, merge several bigWigs into one bedGraph.
    """
    BW_MERGE_TEMPLATE = "bigWigMerge {inBWs} {outBG}"

    def __init__(self, in_bws, out_bgs):
        self.sources = in_bws #list of lists of bw
        self.outputs = out_bgs #list of bg

    def build(self):
        return [
            self.BW_MERGE_TEMPLATE.format(
                inBWs=' '.join(src),
                outBG=out
            )
            for src, out in zip(self.sources, self.outputs)
        ]

class bigWigMergePlus(object):
    """Generate several bigWigMergePlus commands.
    For each command, merge several bigWigs into one bigWig.
    """
    bigwigmergeplus_path = os.path.join(script_dir(), "merge_bigwig/bigWigMergePlus-mp2b")
    bw_merge_plus_template = bigwigmergeplus_path + " {inBWs} {outBG}"

    def __init__(self, in_bws, out_bgs):
        self.sources = in_bws #list of lists of bw
        self.outputs = out_bgs #list of bg

    def build(self):
        return [
            self.bw_merge_plus_template.format(
                inBWs=' '.join(src),
                outBG=out
            )
            for src, out in zip(self.sources, self.outputs)
        ]

class appendMergeCompletion(object):
    """Generate several append commands.
    For each command, append two values to a tsv file.
    """
    APPEND_TEMPLATE = r"printf '{dataset}\t{md5}\n' >> {tsv}"

    def __init__(self, datasets, md5s, tsv_file):
        self.dsets = datasets
        self.md5s = md5s
        self.tsv_file = tsv_file

    def build(self):
        return [
            self.APPEND_TEMPLATE.format(
                dataset=dset,
                md5=md5,
                tsv=self.tsv_file
            )
            for dset, md5 in zip(self.dsets, self.md5s)
        ]

class bigWigToBedGraph(object):
    """Generate several bigWigToBedGraph commands.
    For each command, convert a bigWig to a bedGraph.
    """
    BW_TO_BG_TEMPLATE = "bigWigToBedGraph {inBW} {outBG}"

    def __init__(self, in_bgs, out_bws):
        self.sources = in_bgs
        self.outputs = out_bws

    def build(self):
        return [
            self.BW_TO_BG_TEMPLATE.format(
                inBW=src,
                outBG=out
            )
            for src, out in zip(self.sources, self.outputs)
        ]

class bedGraphToBigWig(object):
    """Generate several bedGraphToBigWig commands.
    For each command, convert a bedGraph to a bigWig.
    """
    BG_TO_BW_TEMPLATE = "bedGraphToBigWig {inBG} {chromSizes} {outBW}"

    def __init__(self, in_bgs, chrom_sizes, out_bws):
        self.sources = in_bgs
        self.chrom_sizes = chrom_sizes
        self.outputs = out_bws

    def build(self):
        return [
            self.BG_TO_BW_TEMPLATE.format(
                inBG=src,
                chromSizes=self.chrom_sizes,
                outBW=out
            )
            for src, out in zip(self.sources, self.outputs)
        ]

class filterBedGraph(object):
    """Generate several filter_bedGraph.py commands.
    For each command, keep only the canonical chromosomes of a bedGraph.
    """
    filter_bedgraph_path = os.path.join(script_dir(), "standardize_bigwig/filter_bedGraph")
    filter_bg_template = filter_bedgraph_path  + " {inBG} {outBG}"

    def __init__(self, in_bgs, out_bgs):
        self.sources = in_bgs
        self.outputs = out_bgs

    def build(self):
        return [
            self.filter_bg_template.format(
                inBG=src,
                outBG=out
            )
            for src, out in zip(self.sources, self.outputs)
        ]
