"""Metadata controlled labels"""

TRACK_TYPE_LABEL = "track_type"
SIGNAL_MERGED = "signal_merged"

STATUS_LABEL = "status"
FINAL_STATUS = "final"
INTER_STATUS = "inter"
UNMERGEABLE_STATUS = "unmergeable"
UNUSABLE_STATUS = "unusable"
MISSING_STATUS = "missing"

MD5SUM_LABEL = "md5sum"
DATASET_LABEL = "dataset_name"

EMPTY = "--"
