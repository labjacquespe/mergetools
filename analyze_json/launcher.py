from __future__ import absolute_import, division, print_function

import os.path
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../..')))

from ihec_json import IhecJson
from epigeec_ihecdata.epigeec_ihecdata import ihecdatasource

def manual_mode(options):
    """Write analysis files to given paths."""

    ihec_json_files = options.json
    report_filepath = options.report
    merge_filepath = options.merge
    flatten_filepath = options.flatten

    if report_filepath is None and merge_filepath is None and flatten_filepath is None:
        raise ValueError("Need at least one optional flag/file to write to. See program help.")

    ihec_json = IhecJson()
    for ihec_json_file in ihec_json_files:
        ihec_json.load_from_file(ihec_json_file)

    if report_filepath is not None:
        with open(report_filepath, 'w') as report_file:
            ihec_json.write_report(report_file)

    if merge_filepath is not None:
        with open(merge_filepath, 'w') as merge_file:
            ihec_json.write_merge_list(merge_file)

    if flatten_filepath is not None:
        with open(flatten_filepath, 'w') as flatten_file:
            ihec_json.write_flattened_json(flatten_file)


def automatic_mode(options):
    """Write analysis files with datasource handler."""

    assembly = options.assembly
    release = options.release
    datapath = options.datapath

    local_src = ihecdatasource.LocalIhecDatasource(assembly, release, datapath)

    ihec_json = IhecJson()
    ihec_json.load_from_object(local_src.read_ihec_json())

    local_src.write_report(ihec_json.make_report())
    local_src.write_flat_json(ihec_json.make_flattened_json())
    local_src.write_merge_info(ihec_json.to_merge)
