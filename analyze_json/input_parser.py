from __future__ import absolute_import, division, print_function

import argparse

import launcher
from mergetools.general_parser_args import add_general_args

def parse_args(args):
    """Define parser and return parsed arguments."""
    parser = argparse.ArgumentParser(prog="analyse_json", description="Analyze a ihec JSON file to collect signal and merge information.")
    subparsers = parser.add_subparsers(help="Sub-command help.")

    parser_manual = subparsers.add_parser("manual", description="Specify files to create.")
    parser_manual.set_defaults(func=launcher.manual_mode)
    parser_manual.add_argument("json", type=argparse.FileType('r'), nargs='+', help="IHEC json file(s).")
    parser_manual.add_argument("--report", type=str, help="File where to write masterTable information (tsv).")
    parser_manual.add_argument("--merge", type=str, help="File where to write md5s to merge. One merge per line (tsv)")
    parser_manual.add_argument("--flatten", type=str, help="File where to write flattened JSON version.")

    parser_auto = subparsers.add_parser("auto", description="Read and create files in an automatic way, for a release/assembly pair.")
    parser_auto.set_defaults(func=launcher.automatic_mode)
    parser_auto = add_general_args(parser_auto)

    return parser.parse_args(args)
