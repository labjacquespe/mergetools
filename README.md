# mergetools

'Merge' IHEC bigWigs into local IHEC data.  

These scripts use content from the `epigeec_ihecdata` project,
and so the `mergetools` project folder needs to be at the same level
as the `epigeec_ihecdata` project folder.  

## Scripts

- analyse_json  

Analyze a ihec JSON file to collect signal and merge information.  

- check_source  

Verify the source files for missing files and incoherent md5sum names.  

- merge_bigwig  

Create scripts to merge bigwigs.  

- metatada_updater  

Update flat JSON with merged signals.  

- cleanup  

Finish up merging/standardize pipeline.  

- standardize_bigwig  

Create scripts to standardize bigWigs structure,
including filter out non canonical chromosomes.  

## Intended pipeline

~~~bash
# OUT OF CURRENT PROJECT (from epigeec_ihecdata): Init folder structure
mkdir <datapath>
ihecdata init [--path PATH] [--resource RESOURCE]

# Create merge.tsv + flat.json
analyze_json [--report REPORT] [--merge MERGE] [--flatten FLATTEN] json

# Verify source files: Add missing.md5 and bad_pairs.tsv
check_source [--verbose] [--cpu] <assembly> <release> <datapath>

# Create merge scripts (launch with sbatch still needed)
merge_bigwig init <assembly> <release> <datapath>
merge_bigwig merge [--verify] <assembly> <release> <datapath>

# Create standardize scripts (launch with sbatch still needed)
standardize_bigwig [--verify] [--from-ihec] [--from-merge] <assembly> <release> <datapath>

# Delete useless files (merge folder and +), and create symlink for merged bigwigs (md5.merge --> md5)
cleanup [--merge] [--empty-merge] [--standardize] <assembly> <release> <datapath>

# Update flat json with merged signals + create final version
metatada_updater [--verify | --from-file] [--final]

# OUT OF CURRENT PROJECT:
# Create and append content to masterTable (from epigeec_mastertable)
# Then hdf5+matrix correlation (from epigeec_generator)
~~~

## Overview of scripts

Both `merge_bigwig` and `standardize_bigwig` create scripts in a `./script` folder.
It is recommended to move the created scripts to a folder where their context
is known better. The scripts can then be launched with `ls *.sh | xargs -n1 sbatch`.  

It is also possible to keep track of the created jobs by copying the
"Submitted batch job JOBID" message to a file and then running the following command:
`grep -Eo '[0-9]+' submitted_jobs.list > submitted_jobid.list && rm submitted_jobs.list`.  

Both programs also use `{assembly}_{release}_valid_merge.md5`
and `{assembly}_{release}_valid_standardize.md5`
to verify which standardizations have already been completed.  

All scripts work on a release/assembly basis. Some .sh files are provided
for convenience to run the scripts on multiple assemblies/releases.  

### analyze_json

Create up to three different files, detailed in the following help.  

`python main.py manual -h`

~~~text
usage: analyse_json manual [-h] [--report REPORT] [--merge MERGE]
                           [--flatten FLATTEN]
                           json [json ...]

Specify files to create.

positional arguments:
  json               IHEC json file(s).

optional arguments:
  -h, --help         show this help message and exit
  --report REPORT    File where to write masterTable information (tsv).
  --merge MERGE      File where to write md5s to merge. One merge per line
                     (tsv)
  --flatten FLATTEN  File where to write flattened JSON version.
~~~

`python main.py auto -h`

~~~text
usage: analyse_json auto [-h]
                         {hg19,hg38,mm10} {2016-11,2017-10,2018-10} datapath

Read and create files in an automatic way, for a release/assembly pair.

positional arguments:
  {hg19,hg38,mm10}      Genome assembly.
  {2016-11,2017-10,2018-10}
                        IHEC data portal build/release.
  datapath              Path where the local IHEC data is located.
~~~

For the automatic mode to work, the original IHEC json
needs to be already present in the local IHEC data.

### check_source

`python check_source.py -h`

~~~text
usage: check_source [-h] [--verbose] [--cpu CPU]
                    {hg19,hg38,mm10} {2016-11,2017-10,2018-10,2019-11}
                    datapath

Verify the source files. Add missing.md5 and bad_pairs.tsv to the local IHEC
datasource.

positional arguments:
  {hg19,hg38,mm10}      Genome assembly.
  {2016-11,2017-10,2018-10,2019-11}
                        IHEC data portal build/release.
  datapath              Path where the local IHEC data is located.

optional arguments:
  -h, --help            show this help message and exit
  --verbose, -v         Print missing md5 and md5 match info.
  --cpu CPU             Number of parallel processes. Default four. Much
                        higher not recommended.
~~~

### merge_bigwig

`python main.py init -h`

~~~text
usage: merge_bigwig init [-h]
                         {hg19,hg38,mm10} {2016-11,2017-10,2018-10} datapath

Create expected merge report (dset new_md5sum) and check for name collisions.

positional arguments:
  {hg19,hg38,mm10}      Genome assembly.
  {2016-11,2017-10,2018-10}
                        IHEC data portal build/release.
  datapath              Path where the local IHEC data is located.
~~~

The init mode needs to be ran first one time. It aborts if there are
name collisions.  

`python main.py merge -h`

~~~text
usage: merge_bigwig merge [-h] [--verify] [--size SIZE] [--mem MEM]
                          [--walltime WALLTIME]
                          {hg19,hg38,mm10} {2016-11,2017-10,2018-10} datapath

Create scripts for merge jobs.

positional arguments:
  {hg19,hg38,mm10}      Genome assembly.
  {2016-11,2017-10,2018-10}
                        IHEC data portal build/release.
  datapath              Path where the local IHEC data is located.

optional arguments:
  -h, --help            show this help message and exit
  --verify              Only give the number of already performed merges.

Job parameters:
  job size and sbatch parameters.

  --size SIZE           Max number of merge commands per scripts. Default=100
  --mem MEM             sbatch mem. Default=24G
  --walltime WALLTIME   sbatch walltime. Default=12h
~~~

The merging is done using  `bigWigMergePlus-mp2b`.  

Since valid.md5 files are always updated (validating bigWigs),
the program will take longer to run when new files have been merged or standardized.  

With `--verify` on, no scripts are created, but the valid.md5 files are still updated.  

The program does not currently handle missing files for merges.
If a file that would be used for a merge is missing from the IHEC mount source,
the program will fail. A way to bypass this problem is to
delete the problematic merge line from `{assembly}_{release}_merge.tsv`.  

### metatada_updater  

`python metadata_updater.py -h`

~~~text
usage: metadata_updater [-h] [--verify | --from-file] [--final]
                        {hg19,hg38,mm10} {2016-11,2017-10,2018-10} datapath

Update flat JSON with merged signals.

positional arguments:
  {hg19,hg38,mm10}      Genome assembly.
  {2016-11,2017-10,2018-10}
                        IHEC data portal build/release.
  datapath              Path where the local IHEC data is located.

optional arguments:
  -h, --help            show this help message and exit
  --verify              Only verify how many valid bigWigs are in the merge
                        folder.
  --from-file           Use the current valid_merge.md5 to update metadata. Do
                        not check bigwigs in the merge folder.
  --final               Create flat json with only available final signals.
~~~

The program does not remove any dataset from the flat json, it only adds new files.  

Use `--final` when the merging process is finished.  

### cleanup

`python cleanup.py -h`

~~~text
usage: cleanup [-h] [--merge] [--empty-merge] [--standardize]
               {hg19,hg38,mm10} {2016-11,2017-10,2018-10} datapath

Finish up merging/standardize pipeline

positional arguments:
  {hg19,hg38,mm10}      Genome assembly.
  {2016-11,2017-10,2018-10}
                        IHEC data portal build/release.
  datapath              Path where the local IHEC data is located.

optional arguments:
  -h, --help            show this help message and exit
  --merge               Empty merged bigWig folder of standardized bigWigs and
                        create symlinks to non .merge versions in bigWig
                        folder.
  --empty-merge         Remove all files from the merge folder, includes
                        --merge.
  --standardize         Remove non valid files from the standardized bigWig
                        folder.
~~~

### standardize_bigwig

`python standardize_bigwig.py -h`

~~~text
usage: standardize_bigwig [-h] [--verify] [--from-merge] [--from-ihec]
                          [--size SIZE] [--mem MEM] [--walltime WALLTIME]
                          {hg19,hg38,mm10} {2016-11,2017-10,2018-10} datapath

Create scripts to standardize bigWigs structure, including filter out non
canonical chromosomes.

positional arguments:
  {hg19,hg38,mm10}      Genome assembly.
  {2016-11,2017-10,2018-10}
                        IHEC data portal build/release.
  datapath              Path where the local IHEC data is located.

optional arguments:
  -h, --help            show this help message and exit
  --verify              Only print what would be performed.

Targets:
  Select files to standardize, at least one target is needed.

  --from-merge          Standardize local merged bigWigs.
  --from-ihec           Standardize IHEC bigWigs.

Job parameters:
  job size and sbatch parameters.

  --size SIZE           Max number of bigWig to standardize per script.
                        Default=100
  --mem MEM             sbatch mem. Default=8G
  --walltime WALLTIME   sbatch walltime. Default=24h
~~~

Since valid.md5 files are always updated (validating bigWigs),
the program will take longer to run when new files have been merged or standardized.  

Each bigWig is standardized with the following pipeline:
`bigWigtoBegGraph --> filter_bedGraph --> bedGraphToBigWig`. Intermediary
bedgraph files are written to the local compute node scratch `$SLURM_TMPDIR`. It is
possible to write to the local datasource instead when a compute node is not used.  

With `--verify` on, no scripts are created, but the valid.md5 files are still updated.  

## Expected problems

Problems encountered during merging/standardize process, with examples.  

- bigWigMergePlus errors  

  The first error does not produce any bigWig file (and occurs in bigWigMerge too), and so it is supposed to be a source IHEC file problem. The second and third errors generate corrupted bigWigs, and seem to be bigWigMergePlus specific problems.  

~~~text
Error: Merging from different assemblies? Chromosome chr1 is 249250621 in /.../encode/hg19/2255c591d4d888e84b96d1dbf5f107cc but 248956422 in another file

bigWigMergePlus-mp2b: bigWigMergePlus.c:668: itemsWriteReducedOnceReturnReducedTwice: Assertion `boundsPt == boundsEnd' failed.

bigWigMergePlus-mp2b: bbiWrite.c:682: bbiOutputOneSummaryFurtherReduce: Assertion `bounds < boundsEnd' failed.
~~~

- bedGraphToBigWig errors  

  The first error is seen when using the tool on merged files and directly from IHEC source files. It also occurs from files merged with bigWigMerge instead of bigWigMergePlus, and so seems to be a source bigWig problem (i.e., independant of the merging process).  

  The second error was only seen on merged bigWigs, and was not handled as it was on a very limited number of files. Furthermore, the bedgraph created by bigWigMergePlus should have been already sorted out of the bigWigMergePlus. It is either a problem with the tool or the source files.  

~~~text
End coordinate 81195840 bigger than chr17 size of 81195210 line 12102477 of /.../c00d314e5233add7d0f781e15549b019.merge.filtered

/.../b796a26b266e45468b7aace28dbf07b1.merge.filtered is not sorted at line 35721779.  Please use "sort -k1,1 -k2,2n" or bedSort and try again.
~~~

- bigWigToBedGraph errors  

  The two errors coming from this tool seem to be source IHEC file problems, as they are not seen when converting merged bigwig files.  

~~~text
/.../kanai/hg19/96aea78d58034854bc2fe9f8e10f9817 is not a big wig file
needLargeMem: trying to allocate 0 bytes (limit: 100000000000)

udc couldn't read 4 bytes from /.../roadmap/hg19/943523e1d88e0d859b4dae4a8cc3d160, did read 0
needLargeMem: trying to allocate 0 bytes (limit: 100000000000)
~~~

To verify what problem occured, once a `--verify` has been ran and not everything seems to have been processed,
the following command can be ran from the script folder. It removes irrelevant lines always printed by bigWigMergePlus.  

~~~bash
grep -vE 'Processing|Got|Writing' *.out > errors.txt
~~~
