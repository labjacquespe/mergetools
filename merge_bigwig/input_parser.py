from __future__ import absolute_import, division, print_function

import argparse
import launcher
from mergetools.general_parser_args import add_general_args

def parse_args(args):

    parser = argparse.ArgumentParser(prog="merge_bigwig", description="Create scripts to merge bigwigs.")
    subparsers = parser.add_subparsers(help="Sub-command help.")

    # Init mode
    parser_init = subparsers.add_parser("init", description="Create expected merge report (dset new_md5sum) and check for name collisions.")
    parser_init.set_defaults(func=launcher.init)
    parser_init = add_general_args(parser_init)

    # Normal mode
    parser_merge = subparsers.add_parser("merge", description="Create scripts for merge jobs.")
    parser_merge.set_defaults(func=launcher.merge)
    parser_merge.add_argument("--verify", action='store_true', help="Only give the number of already performed merges.")
    parser_merge = add_general_args(parser_merge)

    job_param = parser_merge.add_argument_group("Job parameters", "job size and sbatch parameters.")
    job_param.add_argument("--size", type=int, help="Max number of merge commands per scripts. Default=100", default=100)
    job_param.add_argument("--mem", type=str, help="sbatch mem. Default=24G", default="24G")
    job_param.add_argument("--walltime", type=str, help="sbatch walltime. Default=12h", default="12:00:00")

    return parser.parse_args(args)
