"""Launch the program init or merge mode."""
from __future__ import absolute_import, division, print_function

import mergetools.job_info as job_info
from epigeec_ihecdata.epigeec_ihecdata import ihecdatasource

import init_merge
import create_scripts

def init_local_source(options):
    """Initialize and return IHEC local datasources."""
    return ihecdatasource.LocalIhecDatasource(
        assembly=options.assembly,
        release=options.release,
        datapath=options.datapath
        )

def init_mount_source(options):
    """Initialize and return IHEC mount datasources."""
    mount_src = ""
    if options.release == "2019-11":
        mount_src = ihecdatasource.Mp2b2019IhecDatasource(
            assembly=options.assembly,
            release=options.release
        )
    else:
        mount_src = ihecdatasource.Mp2bMountIhecDatasource(
            assembly=options.assembly,
            release=options.release
        )
    return mount_src

def init_job_info(options):
    """Initialize and return job info object."""
    return job_info.JobInfo(options.size, options.walltime, options.mem)

def init(options):
    """Create expected merge report (dset new_md5sum)
    if there are name collisions.
    """
    local_src = init_local_source(options)

    # Create dataset:md5s_to_merge dict from merge_list
    merge_dict = local_src.read_merge_info()

    init_merge.files_proportion_message(merge_dict)

    new_md5s = {dset:init_merge.compute_new_md5(md5s)
                for dset, md5s in merge_dict.items()}

    init_merge.check_for_md5_collisions(
        metadata=local_src.read_flat_json(),
        new_md5s=new_md5s
        )

    local_src.write_expected_merges(new_md5s.items())

def merge(options):
    """Create scripts for remaining merges to perform."""
    only_check = options.verify
    job_params = init_job_info(options)
    local_src = init_local_source(options)
    mount_src = init_mount_source(options)

    # Determine which merge have already been done according to valid.md5 files.
    all_new_md5s = {dset:md5 for dset, md5 in local_src.read_expected_merges()}

    md5_to_merge = create_scripts.find_md5s_to_merge(all_new_md5s.values(), local_src)

    create_scripts.already_merged_message(md5_to_merge, all_new_md5s)

    if only_check:
        print("Check done")
        return

    # Make paths of new files
    new_merge_paths = {dset:local_src.make_merge_path(md5)
                       for dset, md5 in all_new_md5s.items()
                       if md5 in md5_to_merge}

    # Find files to merge. If at least one file is missing, print culprit and skip dset.
    to_merge_paths = {}
    for dset, md5s in local_src.read_merge_info().items():
        try:
            to_merge_paths[dset] = mount_src.get_paths(md5s)
        except IOError as e:
            print("Skipping dataset {}: {}".format(dset, e))
            del new_merge_paths[dset]

    # Prepare paths for job scripts.
    dset_to_merge = new_merge_paths.keys()
    in_bws = [to_merge_paths[dset] for dset in dset_to_merge]
    out_bws = [new_merge_paths[dset] for dset in dset_to_merge]

    # Create job scripts
    create_scripts.make_job_scripts(
        in_bws=in_bws,
        out_bws=out_bws,
        merge_per_script=job_params.size,
        walltime=job_params.wall,
        mem=job_params.mem
    )

    job_info.clean_job_script_message()
