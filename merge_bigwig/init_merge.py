"""Functions exclusive to the init branch of the program."""
from __future__ import absolute_import, division, print_function

import hashlib
import sys

from mergetools.controlled_names import SIGNAL_MERGED, TRACK_TYPE_LABEL, MD5SUM_LABEL

def compute_new_md5(md5s):
    """Return md5 of concatened sorted md5s."""
    concat_md5 = "".join(sorted(md5s))
    return hashlib.md5(concat_md5).hexdigest()

def files_proportion_message(merge_dict):
    """Print some information about files proportions.
    merge_dict is a dataset:md5s_to_merge dict.
    """
    nb_files = sum([len(md5s) for md5s in merge_dict.values()])
    nb_dset = len(merge_dict)
    message = "{nb1} files to merge from {nb2} datasets. Proportion: {prop:.2f}".format(
        nb1=nb_files,
        nb2=nb_dset,
        prop=nb_files/nb_dset
    )
    print(message)

def check_for_md5_collisions(metadata, new_md5s):
    """Check for potential filename collision.
    Print information and exit program if one is present.
    new_md5s is a dataset:new_md5 dict.
    """
    problem = False

    ihec_md5s = set()
    for dset in metadata:
        if dset[TRACK_TYPE_LABEL] != SIGNAL_MERGED:
            ihec_md5s.add(dset[MD5SUM_LABEL])

    for dset_name, md5 in new_md5s.items():
        if md5 in ihec_md5s:
            problem = True
            print("md5 collision present.")
            print("Culprit: {}\t{}".format(dset_name, md5))

    if problem:
        print("Aborted.", file=sys.stderr)
        sys.exit()
