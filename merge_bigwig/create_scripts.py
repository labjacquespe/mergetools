"""Functions exclusive to the merge branch of the program."""
from __future__ import absolute_import, division, print_function

from mergetools.command import bigWigMergePlus
from epigeec_generator import job

def find_md5s_to_merge(new_md5s, local_src):
    """Return a set of which md5s should be merged,
    according to valid.md5 files
    """
    valid_merge_md5s = set(local_src.update_valid_merge())
    valid_standardize_md5s = set(local_src.update_valid_standardize())

    md5_to_merge = set(new_md5s) - valid_merge_md5s - valid_standardize_md5s

    return md5_to_merge

def already_merged_message(new_merges, all_merges):
    """Print a message about already finished merges"""
    message = "{nb1}/{nb2} valid merges already performed.".format(
        nb1=len(all_merges)-len(new_merges),
        nb2=len(all_merges)
    )
    print(message)

def make_job_scripts(in_bws, out_bws, merge_per_script, walltime, mem):
    """Create scripts in ./scripts/, with a given number of merge per script.
    The input and output paths lists need to correspond.
    """
    j = merge_per_script # j for job size
    for i in range(0, len(out_bws), j):
        cmd1 = bigWigMergePlus(in_bws[i:i+j], out_bws[i:i+j])
        merge_job = job.Job(walltime=walltime, mem=mem)
        merge_job.add_command(cmd1).build()
