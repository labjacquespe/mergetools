from __future__ import absolute_import, division, print_function

import os.path
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../..')))

import input_parser

def main(argv):

    args = input_parser.parse_args(argv)
    args.func(args)

def cli():
    main(sys.argv[1:])


if __name__ == "__main__":
    cli()
