#!/bin/bash

datapath="/nfs3_ib/ip29/ip29/jacques_group/local_ihec_data"
ASSEMBLIES=("hg38" "hg19" "mm10")
RELEASES=("2016-11" "2017-10" "2018-10")

# Make sure the datapath does not finish with /.

for assembly in ${ASSEMBLIES[@]}; do
    for release in  ${RELEASES[@]}; do
        echo -e "python main.py merge --verify ${assembly} ${release} ${datapath}\n"
        python main.py merge --verify ${assembly} ${release} ${datapath}
    done
done
