from __future__ import absolute_import, division, print_function

import argparse
import json
import os.path
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../..')))

from epigeec_ihecdata.epigeec_ihecdata import ihecdatasource
from mergetools.controlled_names import MD5SUM_LABEL, DATASET_LABEL
from mergetools.general_parser_args import add_general_args


def parse_args(args):

    parser = argparse.ArgumentParser(prog="add_qc_track_md5sum", description="Update IHEC JSON with qc_track_md5sum attribute for treated datasets.")
    parser = add_general_args(parser)
    parser.add_argument("input_metadata", help="Filepath of IHEC metadata file")
    parser.add_argument("output_metadata", help="Filepath of updated metadata file")
    return parser.parse_args(args)


def main(argv):

    args = parse_args(argv)

    assembly = args.assembly
    release = args.release
    datapath = args.datapath

    in_path = args.input_metadata
    out_path = args.output_metadata

    local_src = ihecdatasource.LocalIhecDatasource(assembly, release, datapath)

    dset_name_to_md5 = {dset[DATASET_LABEL]:dset[MD5SUM_LABEL] for dset in local_src.read_final_json()}
    print("Adding {} treated files md5sum to metadata.".format(len(dset_name_to_md5)))

    with open(in_path, 'r') as in_json:
        meta = json.load(in_json)

    i = 0
    for dset_name in meta["datasets"]:
        if dset_name in dset_name_to_md5:
            i += 1
            meta["datasets"][dset_name]["qc_track_md5sum"] = dset_name_to_md5[dset_name]

    print("{} md5sum added with success.".format(i))

    with open(out_path, 'w') as out_file:
        json.dump(meta, out_file)


def cli():
    main(sys.argv[1:])

if __name__ == "__main__":
    cli()
