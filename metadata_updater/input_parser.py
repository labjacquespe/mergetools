from __future__ import absolute_import, division, print_function

import argparse
from mergetools.general_parser_args import add_general_args

def parse_args(args):

    parser = argparse.ArgumentParser(prog="metadata_updater", description="Update flat JSON with merged signals.")

    parser = add_general_args(parser)

    exclusive = parser.add_mutually_exclusive_group()
    exclusive.add_argument("--verify", action="store_true", help="Only verify how many valid bigWigs are in the merge folder.")
    exclusive.add_argument("--from-file", action="store_true", help="Use the current valid_merge.md5 to update metadata. Do not check bigwigs in the merge folder.")

    parser.add_argument("--final", action="store_true", help="Create flat json with only available final signals.")

    return parser.parse_args(args)
