from __future__ import absolute_import, division, print_function

from collections import Counter
import os.path
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../..')))

from epigeec_ihecdata.epigeec_ihecdata import ihecdatasource
from mergetools.controlled_names import SIGNAL_MERGED, TRACK_TYPE_LABEL, STATUS_LABEL, FINAL_STATUS, MD5SUM_LABEL, DATASET_LABEL
import input_parser

def _reset_id(flat_json):
    """Reset the id attribute of datasets."""
    for i, dset in enumerate(flat_json):
        dset["id"] = i
    return flat_json

def find_old_merges_dset(meta):
    """Return the set of all dset names that have been merged
    and already added to the metadata file.
    """
    return set([dset[DATASET_LABEL]
                for dset in meta
                if dset[TRACK_TYPE_LABEL] == SIGNAL_MERGED])

def create_combined_info(old_merges, new_merges, meta):
    """Return the content of the updated json."""
    # Copy metadata tied to new merges
    # We want only one set of metadata for each merge
    merged_meta = {}
    for dset in meta:
        dset_name = dset[DATASET_LABEL]
        if dset_name in new_merges and dset_name not in old_merges:
            merged_meta[dset_name] = {k:v for k, v in dset.items()}

    # Correct metadata for merged signal
    new_meta = []
    for dset_name, dset in merged_meta.items():
        dset.update({
            MD5SUM_LABEL:new_merges[dset_name],
            STATUS_LABEL:FINAL_STATUS,
            TRACK_TYPE_LABEL:SIGNAL_MERGED
        })
        new_meta.append(dset)

    combined_json = meta + new_meta

    combined_json = _reset_id(combined_json)

    return combined_json

def get_new_merges(expected_merges, merged_md5s):
    """Return dset:md5 dict for merged md5s."""
    return {dset:md5
            for dset, md5 in expected_merges
            if md5 in merged_md5s}

def from_folders_content(meta, old_merges, expected_merges, local_src, only_check):
    """Execute program as normally intended, using current merged bigWigs."""
    merge_md5s = set(local_src.current_mergename_md5s)
    valid_merge_md5s = set(local_src.update_valid_merge())

    # both valid and present in mn
    new_md5s = merge_md5s & valid_merge_md5s

    print("{}/{} valid bigWigs in mn folder".format(len(new_md5s), len(merge_md5s)))

    # get dset:merged md5 dict for valid bw
    new_merges = get_new_merges(expected_merges, new_md5s)

    if not new_merges:
        print("Nothing to update.")
        return

    if only_check:
        print("Check done.")
        return

    # Update old json
    print("Updating metadata file with merged signals.")
    combined_info = create_combined_info(old_merges, new_merges, meta)
    local_src.write_flat_json(combined_info)

def from_file_content(meta, old_merges, expected_merges, local_src):
    """Update metadata by using the valid_merge.md5 file, useful
    if bigWigs are not present in the merge folder anymore
    but the metadata file needs to be re-updated.
    """
    merged_md5s = local_src.read_valid_merge()

    new_merges = get_new_merges(expected_merges, merged_md5s)

    combined_info = create_combined_info(old_merges, new_merges, meta)

    print("Updating metadata file using valid_merge.md5.")
    local_src.write_flat_json(combined_info)

def filter_final(flat_metadata, valid_final_md5s):
    """Return metatada with only validated final signals."""
    filtered_metatada = [dset for dset in flat_metadata
                         if dset[STATUS_LABEL] == FINAL_STATUS
                         and dset[MD5SUM_LABEL] in valid_final_md5s]

    for dset in filtered_metatada:
        del dset[STATUS_LABEL]

    return _reset_id(filtered_metatada)

def print_repeated_md5(flat_metatada):
    """Print repeated md5s in the given metadata."""
    md5_counter = Counter()
    for dset in flat_metatada:
        md5_counter[dset[MD5SUM_LABEL]] += 1

    for md5, nb_md5 in md5_counter.most_common():
        if nb_md5 > 1:
            print(md5, nb_md5)
        else:
            break

def main(argv):

    args = input_parser.parse_args(argv)
    assembly = args.assembly
    release = args.release
    datapath = args.datapath
    only_check = args.verify
    from_file = args.from_file
    make_final = args.final

    # Init datasource.
    local_src = ihecdatasource.LocalIhecDatasource(assembly, release, datapath)

    # Load metadata
    meta = local_src.read_flat_json()

    old_merges = find_old_merges_dset(meta)

    # Load expected merges tuples: (dataset, md5).
    expected_merges = local_src.read_expected_merges()

    if from_file:
        from_file_content(meta, old_merges, expected_merges, local_src)
    else:
        from_folders_content(meta, old_merges, expected_merges, local_src, only_check)

    if make_final:
        print("Creating final signals json.")
        new_meta = local_src.read_flat_json()
        valid_md5s = local_src.read_valid_standardize()
        final_meta = filter_final(new_meta, valid_md5s)

        print("Repeated md5s in final metadata:")
        print_repeated_md5(final_meta)

        print("Writing final json.")
        print("{} datasets written to final json".format(len(final_meta)))
        local_src.write_final_json(final_meta)


def cli():
    main(sys.argv[1:])

if __name__ == "__main__":
    cli()
