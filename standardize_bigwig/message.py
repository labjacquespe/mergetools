from __future__ import absolute_import, division, print_function

def valid_bigwigs(nb1, nb2, source_name):
    """Print message"""
    print("{}/{} valid standardized files from {}".format(nb1, nb2, source_name))

def no_work(source_name):
    """Print message"""
    print("No {} to standardize.".format(source_name))

def verif_done(source_name):
    """Print message"""
    print("{} verification done.".format(source_name))

def all_missing(source_name):
    """Print message"""
    print("All {} files to standardize missing, cannot create jobs.".format(source_name))

def files_to_standardize_present(nb1, nb2, source_name):
    """Print message"""
    print("{}/{} {} files to standardize present.".format(nb1, nb2, source_name))
