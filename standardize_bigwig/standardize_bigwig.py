from __future__ import absolute_import, division, print_function

import os.path
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../..')))

#from parent directory
import mergetools.job_info as job_info_mod
import mergetools.command as command
from mergetools.controlled_names import SIGNAL_MERGED, TRACK_TYPE_LABEL, STATUS_LABEL, FINAL_STATUS, MD5SUM_LABEL

#from other projects
from epigeec_ihecdata.epigeec_ihecdata import ihecdatasource
from epigeec_generator import job

#from current directory
import input_parser
import message

LSCRATCH_PATH = "$SLURM_TMPDIR"

def _make_lscratch_paths(basenames):
    """Return paths of basenames to local scratch."""
    return ["{}/{}".format(LSCRATCH_PATH, name) for name in basenames]

def _make_job_scripts(src_bws, local_src, job_info):
    """Create scripts in ./scripts/, with a given number of merge per script."""
    print("Making scripts\n")
    chrom_file = local_src.get_chromsize("can")

    # keep the potential .merge in the basenames
    basenames = [os.path.basename(path) for path in src_bws]

    # j for job_size
    j = job_info.size
    for i in range(0, len(src_bws), j):

        in_bws = src_bws[i:i+j]
        basenames = [os.path.basename(path) for path in in_bws]

        bgs = _make_lscratch_paths(basenames)
        filt_bgs = ["{}.filtered".format(path) for path in bgs]
        out_bws = local_src.make_bigwig_paths(basenames)

        cmd1 = command.bigWigToBedGraph(in_bws, bgs)
        cmd2 = command.filterBedGraph(bgs, filt_bgs)
        cmd3 = command.bedGraphToBigWig(filt_bgs, chrom_file, out_bws)

        standardize_job = job.Job(
            walltime=job_info.wall,
            mem=job_info.mem
        )
        for cmd in [cmd1, cmd2, cmd3]:
            standardize_job.add_command(cmd)

        standardize_job.build_alternate()

def _find_remaining_md5s(target_md5, local_src, only_check, source_name):
    """Return the remaining md5s to standardize.
    If there are none or only a check is asked, return False.
    """
    verified_md5s = set(local_src.update_valid_standardize())

    # The missing md5s file only affects the mount source process,
    # as it only contains missing md5 from the mount source
    missing_md5s = set(local_src.read_missing_md5s())
    if missing_md5s and source_name == "mount source bigWigs":
        print("{} missing/inexistant source files".format(len(missing_md5s)))

    md5s_to_treat = target_md5 - verified_md5s - missing_md5s

    nb_already_treated = len(verified_md5s & target_md5)

    message.valid_bigwigs(nb_already_treated, len(target_md5), source_name)

    if not md5s_to_treat:
        message.no_work(source_name)
        return False

    if only_check:
        message.verif_done(source_name)
        return False

    return md5s_to_treat

def standardize_merge_bw(local_src, only_check, job_info):
    """Produce scripts (or just message) to standardize merge bigWigs.
    Return True if a script was created, False otherwise.
    """
    SOURCE = "merged bigWigs"
    # All md5s from current merge source to potentially standardize
    merge_md5s = set(local_src.update_valid_merge())

    # Find the md5 of remaining files to standardize,
    # even if said files do not exist anymore
    md5s_to_treat = _find_remaining_md5s(
        target_md5=merge_md5s,
        local_src=local_src,
        only_check=only_check,
        source_name=SOURCE
    )

    # No work to do?
    if not md5s_to_treat:
        return False

    # Now find the paths of existing files to standardize
    merged_bws = local_src.md5_path_dict(local_src.current_mergenames)
    to_standardize = [path for md5, path in merged_bws.items()
                      if md5 in md5s_to_treat]

    # Verify if missing files correctly accounted for
    if not to_standardize:
        message.all_missing(SOURCE)
        return False

    message.files_to_standardize_present(
        nb1=len(to_standardize),
        nb2=len(md5s_to_treat),
        source_name=SOURCE
    )

    #Now create the scripts
    _make_job_scripts(to_standardize, local_src, job_info)

    return True

def _find_ihec_final(flat_json):
    """Return md5s of final files that are not from merges."""
    return set(dset[MD5SUM_LABEL] for dset in flat_json if _is_final_ihec_file(dset))

def _is_final_ihec_file(dset):
    """Return if the dataset if associated with a non-merged final file."""
    return dset[STATUS_LABEL] == FINAL_STATUS and dset[TRACK_TYPE_LABEL] != SIGNAL_MERGED

def standardize_ihec_bw(local_src, mount_src, only_check, job_info):
    """Produce scripts (or just message) to standardize IHEC bigWigs.
    Return True if a script was created, False otherwise.
    """
    SOURCE = "mount source bigWigs"
    # All md5s from ihec source to standardize
    final_md5s = _find_ihec_final(local_src.read_flat_json())

    # Find the md5 of remaining files to standardize
    md5s_to_treat = _find_remaining_md5s(
        target_md5=final_md5s,
        local_src=local_src,
        only_check=only_check,
        source_name=SOURCE
    )
    # No work to do?
    if not md5s_to_treat:
        return False

    # Now find the paths of existing files to standardize
    to_standardize = mount_src.get_paths(md5s_to_treat)

    # Verify if missing files correctly accounted for
    if not to_standardize:
        message.all_missing(SOURCE)
        return False

    nb1 = len(to_standardize)
    nb2 = len(md5s_to_treat)
    message.files_to_standardize_present(
        nb1=nb1,
        nb2=nb2,
        source_name=SOURCE
    )

    if nb1 != nb2:
        s = ("Missing files already accounted for, "
             "the number of files to standardize should match "
             "the number of md5s to treat."
            )
        raise AssertionError(s)

    #Now create the scripts
    _make_job_scripts(to_standardize, local_src, job_info)

    return True

def main(argv):
    """Produce scripts to standardize bigWigs."""
    args = input_parser.parse_args(argv)
    assembly = args.assembly
    release = args.release
    datapath = args.datapath
    only_check = args.verify
    from_ihec = args.from_ihec
    from_merge = args.from_merge
    job_size = args.size
    job_mem = args.mem
    job_walltime = args.walltime

    if not from_ihec and not from_merge:
        print("No standardization target given. Select a target, see help with -h.")
        return

    job_info = job_info_mod.JobInfo(
        size=job_size,
        walltime=job_walltime,
        memory=job_mem
    )

    local_src = ihecdatasource.LocalIhecDatasource(assembly, release, datapath)

    did_work_ihec = None
    if from_ihec:
        if release == "2019-11":
            mount_src = ihecdatasource.Mp2b2019IhecDatasource(assembly, release)
        else:
            mount_src = ihecdatasource.Mp2bMountIhecDatasource(assembly, release)
        did_work_ihec = standardize_ihec_bw(local_src, mount_src, only_check, job_info)

    did_work_merge = None
    if from_merge:
        did_work_merge = standardize_merge_bw(local_src, only_check, job_info)

    if did_work_ihec or did_work_merge:
        job_info_mod.clean_job_script_message()


def cli():
    main(sys.argv[1:])

if __name__ == "__main__":
    cli()
