#include <fstream>
#include <unordered_set> 

int main(int argc, char** argv){

    const std::unordered_set<std::string> chromSet = {"chr1", "chr2", "chr3", "chr4", "chr5", "chr6",
                                           "chr7", "chrX", "chr8", "chr9", "chr11", "chr10",
                                           "chr12", "chr13", "chr14", "chr15", "chr16", "chr17",
                                           "chr18", "chr20", "chr19", "chrY", "chr22", "chr21"};

    std::ifstream infile(argv[1]);
    std::ofstream outfile(argv[2]);
    
    std::string chrom, start, end, val;
    while (infile >> chrom >> start >> end >> val) {
        if (chromSet.find(chrom) != chromSet.end()) {
            outfile << chrom << "\t" << start << "\t" << end << "\t" << val << "\n";
        }
    }
    outfile.close();
}