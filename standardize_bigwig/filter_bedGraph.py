"""
Filter a bedgraph to only keep canonical chromosomes.
Write the filtered version to given path.
"""
import sys

CAN_CHROMS = frozenset(["chr1", "chr2", "chr3", "chr4", "chr5", "chr6",
                        "chr7", "chrX", "chr8", "chr9", "chr11", "chr10",
                        "chr12", "chr13", "chr14", "chr15", "chr16", "chr17",
                        "chr18", "chr20", "chr19", "chrY", "chr22", "chr21"])

def main():

    in_bg_path = sys.argv[1]
    out_bg_path = sys.argv[2]

    out_bg = open(out_bg_path, 'w')
    with open(in_bg_path, 'r') as in_bg:
        for line in in_bg:
            if line.split()[0] in CAN_CHROMS:
                out_bg.write(line)
    out_bg.close()


if __name__ == "__main__":
    main()
