from __future__ import absolute_import, division, print_function

import argparse
from mergetools.general_parser_args import add_general_args

def parse_args(args):

    parser = argparse.ArgumentParser(prog="standardize_bigwig", description="Create scripts to standardize bigWigs structure, including filter out non canonical chromosomes.")

    parser = add_general_args(parser)
    parser.add_argument("--verify", action="store_true", help="Only print what would be performed.")

    target_group = parser.add_argument_group("Targets", "Select files to standardize, at least one target is needed.")
    target_group.add_argument("--from-merge", action="store_true", help="Standardize local merged bigWigs.")
    target_group.add_argument("--from-ihec", action="store_true", help="Standardize IHEC bigWigs.")

    job_param = parser.add_argument_group("Job parameters", "job size and sbatch parameters.")
    job_param.add_argument("--size", type=int, help="Max number of bigWig to standardize per script. Default=100", default=100)
    job_param.add_argument("--mem", type=str, help="sbatch mem. Default=8G", default="8G")
    job_param.add_argument("--walltime", type=str, help="sbatch walltime. Default=24h", default="24:00:00")

    return parser.parse_args(args)
