from __future__ import absolute_import, division, print_function

import argparse
import hashlib
import sys
import os
from multiprocessing import Pool

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../..')))

from mergetools.general_parser_args import add_general_args
from epigeec_ihecdata.epigeec_ihecdata import ihecdatasource


def parse_args(args):
    parser = argparse.ArgumentParser(prog="check_source", description="Verify the source files. Add missing.md5 and bad_pairs.tsv to the local IHEC datasource.")
    parser = add_general_args(parser)
    parser.add_argument("--verbose", "-v", action="store_true", help="Print missing md5 and md5 match info.")
    parser.add_argument("--cpu", type=int, default=4, help="Number of parallel processes. Default four. Much higher not recommended.")
    return parser.parse_args(args)

def compute_file_md5(fname):
    """Return the md5sum of a given file."""
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

def get_used_src_md5s(flat_json):
    """Return the set of all md5s from non-merged
    signals that will be used, according
    to the IHEC metadata.
    """
    used_md5s = set(dset["md5sum"] for dset in flat_json
                    if dset["track_type"] != "signal_merged")

    used_md5s = try_remove(used_md5s, ["", "--", None, "None"])

    return used_md5s

def find_files(ihecdatas):
    """Return the list of signal paths and the list
    of missing files/md5.
    """
    paths = []
    missing_md5s = []
    for ihecdata in ihecdatas:
        if ihecdata.found:
            paths.append(ihecdata.uri)
        else:
            missing_md5s.append(ihecdata.basename)

    return paths, missing_md5s

def get_md5s(path):
    """Return a list of [basename, computed_md5] for given signal path."""
    current_md5 = os.path.basename(path)
    computed_md5 = compute_file_md5(path)
    return [current_md5, computed_md5]

def try_remove(a_set, element_list):
    """Return a set with a removed elements if present."""
    for element in element_list:
        try:
            a_set.remove(element)
        except KeyError:
            pass
    return a_set


def main(args):

    options = parse_args(args)
    assembly = options.assembly
    release = options.release
    datapath = options.datapath
    verbose = options.verbose
    nb_cpu = options.cpu

    mount_src = ""
    if release == "2019-11":
        mount_src = ihecdatasource.Mp2b2019IhecDatasource(assembly, release)
    else:
        mount_src = ihecdatasource.Mp2bMountIhecDatasource(assembly, release)

    local_src = ihecdatasource.LocalIhecDatasource(assembly, release, datapath)

    used_md5s = get_used_src_md5s(local_src.read_flat_json())

    signals, missing_md5s = find_files(mount_src.get_ihecdatas(used_md5s))

    if verbose:
        print("{} files are missing.".format(len(missing_md5s)))
    local_src.write_missing_md5s(missing_md5s)

    pool = Pool(processes=nb_cpu)
    md5_pairs = pool.map(get_md5s, signals)
    bad_pairs = [pair for pair in md5_pairs if pair[0] != pair[1]]

    if verbose:
        print("{} md5s do not match.".format(len(bad_pairs)))
    local_src.write_bad_md5(bad_pairs)

if __name__ == "__main__":
    main(sys.argv[1:])
