from __future__ import absolute_import, division, print_function

class JobInfo(object):
    """Container for some information used to create job scripts"""
    def __init__(self, size, walltime, memory):
        # number of actions per file, 1 action can be several commands
        # if using Job.build_alternate later
        self.size = int(size)
        self.wall = walltime
        self.mem = memory

def clean_job_script_message():
    """Print command to clean .sh scripts"""
    cmd = r"sed -i -e '/^export /d' -e '/^source /d' -e '/^\(# \)\?ml /d' ./script/*.sh"
    print("Run following command on created scripts to",
          "remove export/ml/source lines:\n{}".format(cmd))
