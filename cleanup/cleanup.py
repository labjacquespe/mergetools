from __future__ import absolute_import, division, print_function

import os
import os.path
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../..')))

from epigeec_ihecdata.epigeec_ihecdata import ihecdatasource
import input_parser

def remove_merge(current_merge, md5s_to_delete):
    """Remove merged bigWigs to delete."""
    for md5, path in current_merge.items():
        if md5 in md5s_to_delete:
            os.remove(path)

def create_symlink(folder, md5s):
    """Create symlinks (md5 --> md5.merge) for given md5s."""
    for md5 in md5s:
        symlink_name = os.path.join(folder, md5)
        merge_name = "{}.merge".format(symlink_name)
        try:
            os.symlink(merge_name, symlink_name)
        # link already created
        except OSError:
            pass

def main(argv):
    """Empty merged bigWig folder of standardized bigWigs
    and create symlinks to non .merge versions in bigWig folder.
    """
    args = input_parser.parse_args(argv)
    assembly = args.assembly
    release = args.release
    datapath = args.datapath
    clean_merge_folder = args.merge
    empty_merge_folder = args.empty_merge
    clean_bw_folder = args.standardize

    if empty_merge_folder:
        clean_merge_folder = True

    # Init datasource
    local_src = ihecdatasource.LocalIhecDatasource(assembly, release, datapath)

    # Update valid md5s
    valid_merge_md5 = set(local_src.update_valid_merge())
    valid_standardize_md5 = set(local_src.update_valid_standardize())

    if clean_merge_folder:
        standardized_merge_md5 = valid_merge_md5 & valid_standardize_md5

        remove_merge(
            current_merge=local_src.md5_path_dict(local_src.current_mergenames),
            md5s_to_delete=standardized_merge_md5
            )

        if empty_merge_folder:
            for path in local_src.current_mergenames:
                os.remove(path)

        # Create symlinks for .merge final bigWigs
        create_symlink(local_src.bigwig_dir, standardized_merge_md5)

    if clean_bw_folder:
        current_bigwigs = local_src.md5_path_dict(local_src.current_bigwigs)
        for md5, path in current_bigwigs.items():
            if md5 not in valid_standardize_md5:
                os.remove(path)

def cli():
    main(sys.argv[1:])

if __name__ == "__main__":
    cli()
