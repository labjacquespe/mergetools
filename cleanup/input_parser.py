from __future__ import absolute_import, division, print_function

import argparse
from mergetools.general_parser_args import add_general_args

def parse_args(args):

    parser = argparse.ArgumentParser(prog="cleanup", description="Finish up merging/standardize pipeline.")

    parser = add_general_args(parser)
    parser.add_argument("--merge", action='store_true', help="Empty merged bigWig folder of standardized bigWigs and create symlinks to non .merge versions in bigWig folder.")
    parser.add_argument("--empty-merge", action='store_true', help="Remove all files from the merge folder, includes --merge.")
    parser.add_argument("--standardize", action='store_true', help="Remove non valid files from the standardized bigWig folder.")

    return parser.parse_args(args)
